import React from 'react';
import { Tabs, Icon } from 'antd';
import { FileContent } from './index';

const { TabPane } = Tabs;

function mapPanes(panes, onClose) {
  return panes.map(item => 
    <TabPane 
      tab={<div>{item.name} <Icon type="close-square"  onClick={() => onClose(item.id)}/></div>} 
      key={item.id} 
    >
      <FileContent {...item} />
    </TabPane>);
}

function FileTaps({files, onClose}) {
  return (
    <Tabs 
      defaultActiveKey="0" 
      size='large' 
      animated={false}
    >
      {mapPanes(files, onClose)}
    </Tabs>
  );
}

export default FileTaps;