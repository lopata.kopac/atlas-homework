import React from 'react';
import { Typography } from 'antd';
import './file-content.css';

const { Title, Text  } = Typography;

function FileContent({name, id, text}){
    return(
    <div className='file-item'>
        <Title level={2}>{name}</Title>
        <Text >{text}</Text >
    </div>);
}

export default FileContent;