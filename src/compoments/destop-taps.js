import React, { useState } from 'react';
import { Tabs, Icon, Layout } from 'antd';
import Button from 'antd/es/button';
import './destop-taps.css';

const { Content } = Layout;

const { TabPane } = Tabs;

function mapPanes(panes, onClose) {
  return panes.map((pane, index) => 
    <TabPane 
      tab={<div>{pane.name} <Icon type="close-square"  onClick={() => onClose(index)}/></div>} 
      key={index} 
      
    >
      <div>{pane.content}</div>
    </TabPane>);
}

function DestopTaps({children}) {
  const [panes, setPane] = useState({list: [{name: 'Destiop 1', content: children}]});

  const onClose = (i) => {
      setPane({list: panes.list.filter((item,index) => index !== i)});
  }

  const onAdd = () => {
    setPane({list: panes.list.concat({name: `Destop ${panes.list.length}`, content:  children})});
  }

  return (
    <Layout>
      <Content >
        <Tabs 
          defaultActiveKey="0" 
          tabBarExtraContent={<Button className='plus-button' onClick={onAdd}><Icon  type="plus"/></Button>}
          size='large' 
          animated={false}
          style={{backgroundColor: 'white', height: '100%'}}
        >
          {mapPanes(panes.list, onClose)}
        </Tabs>
      </Content>
    </Layout>
  );
}

export default DestopTaps;