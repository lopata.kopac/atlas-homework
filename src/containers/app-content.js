import React, {useContext} from 'react';
import {  Col, Row, Card, Layout } from 'antd';
import AplikacionContext from '../context/aplikacion';
import { FileTaps } from '../compoments/index';
import { Folder, History } from './index';
import './app-content.css';

const { Content } = Layout;

function AppContent({children}) {
const {openedFiles, closeFile} = useContext(AplikacionContext);

return (
    <Layout>
        <Content >
            <Col span={12}>
                <Row>
                    <Col span={8} >
                        <History />
                    </Col>
                    <Col span={16}>
                        <Card className='folder'><Folder /></Card>
                    </Col>
                </Row>
            </Col>
            <Col span={12}>
                <FileTaps files={openedFiles} onClose={closeFile}/>
            </Col>
        </Content>
    </Layout>
    );
}

export default AppContent;