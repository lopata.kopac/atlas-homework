import React, { useContext } from 'react';
import AplikacionContext from '../context/aplikacion';
import {Card} from 'antd';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import './file.css';

const dataById = id => gql`{
    getFile(id: "${id}") {
        id
        name
        text
        }
}`;

function File({id, name, type}){
    const { openFile } = useContext(AplikacionContext);
    const { loading, error, data } = useQuery(dataById(id));

    if(loading) return <div>Loading</div>;

    if(error) return <div>Error :(</div>;

    return(<Card className='file-item-card' onClick={() => openFile({...data.getFile})}>{name}</Card>);
}

export default File;